//package com.alex.web.chat.config.sendtouser;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.Payload;
//import org.springframework.messaging.simp.annotation.SendToUser;
//import org.springframework.stereotype.Controller;
//
//import java.security.Principal;
//import java.util.Map;
//
//@Controller
//public class WebsocketSendToUserController {
//
//    @Autowired
//    ObjectMapper objectMapper;
//
//    @MessageMapping("/message")
//    @SendToUser("/queue/reply")
//    public String processMessageFromClient(@Payload String message, Principal principal) throws Exception {
//        return objectMapper.readValue(message, Map.class).get("name").toString();
//    }
//
//    @MessageExceptionHandler
//    @SendToUser("/queue/errors")
//    public String handleException(Throwable exception) {
//        return exception.getMessage();
//    }
//}
