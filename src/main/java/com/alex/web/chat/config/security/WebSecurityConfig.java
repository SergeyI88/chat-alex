package com.alex.web.chat.config.security;

import com.alex.web.chat.config.security.filter.JwtFilter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;

@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    final AuthenticationProvider userAuthenticationProvider;
    final UserDetailsService userPrincipalDetailsService;

    public WebSecurityConfig(AuthenticationProvider userAuthenticationProvider, UserDetailsService userPrincipalDetailsService) {
        this.userAuthenticationProvider = userAuthenticationProvider;
        this.userPrincipalDetailsService = userPrincipalDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("admin"))
                .roles("ADMIN", "USER");
        auth.userDetailsService(userPrincipalDetailsService).passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder());
        auth.authenticationProvider(userAuthenticationProvider);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .mvcMatchers("/resources/**")
                .antMatchers(HttpMethod.OPTIONS, "/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/rest-api/**").authenticated()
                .and().formLogin().loginPage("/authorization")
                .defaultSuccessUrl("/chat")
//                .and().logout().logoutUrl("/")
                .and().authorizeRequests().antMatchers("/inner-api/**").hasAuthority("ROLE_ADMIN")
                .and().httpBasic()
                .and().csrf().disable();
        JwtFilter filter = new JwtFilter("/rest-api/**");
        filter.setAuthenticationManager(authenticationManager());
//        http.addFilterAfter(filter, BasicAuthenticationFilter.class);
    }
}
